import cherrypy

class Server:
    @cherrypy.expose
    def index(self):
        return "Hello world!"

cherrypy.quickstart(Server, '/', config={'global':{'server.socket_host': '10.0.0.4', 'server.socket_port': 8080}})
