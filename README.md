# cryptohexes

Blockchain system based on multichain to facilitate tracking attendance/demo hexagons from hackathons, and possibly more!

## API Documentation

Tables represent dictionaries/JSON objects.

### Error Reporting
Every response contains an error code, and optionally an error message:

Name | Type
---- | ----
`error_code` | `integer`
`error_msg` | `string`

Error codes:

Error Code | Description
---------- | -----------
0 | success
1 | incorrectly encoded body
2 | expected body element not found
3 | user not registered
4 | server data malformatted
5 | username taken
6 | unable to find the one-time key

### /api/search

Returns the best `n_results` matches over the profile stream with key `query_string`.

Name | Type
---- | ----
`query_string` | `string`
`n_results` | `integer`

Results: `[(String: name, String: display_name, String: type)]`

`type` is `"hack"`, `"profile"` or `"organiser"`.

### /api/register\_user

Name | Type
---- | ----
`username` | `string`
`password` | `string`

Result: Success (error code 0)

### /api/login

Name | Type
---- | ----
`username` | `string`
`password` | `string`

Result:

Name | Type
---- | ----
`token` | `string`

### /api/get\_profile

Name | Type
---- | ----
`username` | `string`

Result:

Name | Type
---- | ----
`display_name` | `string`
`email` | `string`
`social_media` | `[string]`
`git` | `[string]`
`bio` | `string`
`pic` | `image`
`hackathons` | `[(string display_name, string user_name)]`
`hacks` | `[(string display_name, string hack_name, string organiser_name, string organiser_display_name, bool verified)]`

### /api/set\_profile

Name | Name (`new_profile`) | Type
---- | -------------------- | ----
`token` | | `string`
`new_profile` | `display_name` | `string`
\- | `email` | `string`
\- | `social_media` | `[string]`
\- | `git` | `[string]`
\- | `bio` | `string`
\- | `pic` | `image`
\- | `hacks` | `[(string display_name, string hack_name, string organiser_name)]`

## Streams

### key

Maps a `user_name` or `organiser_name` to immutable properties about that user/organiser. Editable by admin (bot) only.

Key: String `user_name`/`organiser_name`

Body (CBOR encoded):

Name | Type
---- | ----
`encrypted_key` | `string`
`encrypted_name` | `string` (NOTE: `user_name` not `display_name`)
`wallet_addr` | `bytes`
`expiry` | `integer` (for organisers only, UNIX time beyond which assets cannot be transferred)

### profile

Maps `user_name` to its profile information. Editable by organisers and users.

Key: String `user_name`

Body (CBOR encoded):

Name | Type
---- | ----
`display_name` | `string`
`email` | `string`
`social_media` | `[string]`
`git` | `[string]`
`bio` | `string`
`pic` | `image`
`hacks` | `[(string display_name, string hack_name, string organiser_name)]`

### hack

Maps `hack_name` to the hack information. Editable by organisers and users.

Key: String `hack_name`

Body (CBOR encoded):

Name | Type
---- | ----
`display_name` | `string`
`collaborators` | `[string]` (list of `user_name` NOT `display_name`)
`organiser` | `string`
`repo` | `string`
`description` | `string`

### organiser

Maps `oragniser_name` to the organiser profile information. Editable by organisers.

Key: String `organiser_name`

Body (CBOR encoded):

Name | Type
---- | ----
`display_name` | `string`
`email` | `string`
`social_media` | `[string]`
`git` | `[string]`
`bio` | `string`
`pic` | `image`
`hacks` | `[(string display_name, string hack_name, string organiser_name)]`
