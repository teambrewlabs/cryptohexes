import argparse
from pathlib import Path
import Savoir, cbor
from modules import readRPC, searchchain, hexConv

parser = argparse.ArgumentParser(description="Set up an existing multichain blockchain with some demo data for testing.")
parser.add_argument('--chainname', '-c', dest="chainname", default='cryptohex', help="Name of the multichain to be used.")
parser.add_argument('--host', '-o', dest="rpchost", default='localhost', help="Host of the multichain to be used.")
parser.add_argument('--port', '-p', dest="rpcport", default='2839', help="Port of the multichain to be used.")
parser.add_argument('-n', dest="nopublish", action='store_true', help="Don't add any new data to the streams.")

args = parser.parse_args()

# Connect to the multichain instance
path = Path.home() / ".multichain" / args.chainname / "multichain.conf"
(rpcuser, rpcpasswd) = readRPC.read(str(path))
api = Savoir.Savoir(rpcuser, rpcpasswd, args.rpchost, args.rpcport, args.chainname)

hacks = [('0', "IoTea"), ('1', "Phone Flash"), ('2', "Crypto Hex"), ('3', "Project Name Here")]
profiles = [('0', "Shaz"), ('1', "Edd"), ('2', "George"), ('3', "Josh"), ('4', "Zucc")]
organisers = [('0', "MLH"), ('1', "Someone Else")]

if not args.nopublish:
#    for name in ['key', 'profile', 'hack', 'organiser']:
#        api.create('stream', name, False)
#        api.subscribe(name)

    for i in range(len(hacks)):
        api.publish('hack', hacks[i][0], hexConv.dict_to_CBOR({'display_name': hacks[i][1]}))

    for i in range(len(profiles)):
        api.publish('profile', profiles[i][0], hexConv.dict_to_CBOR({'display_name': profiles[i][1]}))

    for i in range(len(organisers)):
        api.publish('organiser', organisers[i][0], hexConv.dict_to_CBOR({'display_name': organisers[i][1]}))

print(searchchain.search(api, "Zucc", 3))
print(searchchain.search(api, "Zuccc", 3))
print(searchchain.search(api, "Crypto Hex", 3))
print(searchchain.search(api, "Shazza", 3))
