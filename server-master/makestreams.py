import argparse
from pathlib import Path
import Savoir
from time import sleep
#from modules import readRPC

# Copied from readRPC because it refused to import
def read(loc):
    lines = open(loc).readlines()
    for line in lines:
        if line.startswith("rpcuser="):
            rpcuser = line[len("rpcuser="):-1]
        if line.startswith("rpcpassword="):
            rpcpasswd = line[len("rpcpassword="):-1]
    return((rpcuser,rpcpasswd))

parser = argparse.ArgumentParser(description="Set up an existing multichain blockchain with some demo data for testing.")
parser.add_argument('--chainname', '-c', dest="chainname", default='cryptohex', help="Name of the multichain to be used.")
parser.add_argument('--host', '-o', dest="rpchost", default='localhost', help="Host of the multichain to be used.")
parser.add_argument('--port', '-p', dest="rpcport", default='2839', help="Port of the multichain to be used.")

args = parser.parse_args()

sleep(5)

# Connect to the multichain instance
path = Path.home() / ".multichain" / args.chainname / "multichain.conf"
(rpcuser, rpcpasswd) = read(str(path))
api = Savoir.Savoir(rpcuser, rpcpasswd, args.rpchost, args.rpcport, args.chainname)

sleep(5)

for name in ['key', 'profile', 'hack', 'organiser', 'hex']:
    sleep(1)
    api.create('stream', name, False)
    sleep(1)
    api.subscribe(name)
