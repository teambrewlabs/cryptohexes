# Setting up our custom multichain on the server

1. Run `multichain-install` as root to install multichain with no configuration.
2. Run `multichain-configure` to set up the basic parameters of the `cryptohex` multichain.
