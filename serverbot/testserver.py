if __name__ == '__main__':  
    from servertest import *

    register_user("shaz", "hunter1")
    register_user("edd", "hunter2")
    register_user("zuckerberg", "werunads")

    register_organiser("HackHarvard", "betterthanmit", "abcde")

    login("shaz", "hunter1")
    login("edd", "hunter2")
    login("zuckerberg", "werunads")


    token = login("shaz", "hunter1")["token"]
    set_profile(token, { 
        "display_name": "Sharan Maiya",
        "email": "sharan98m@gmail.com",
        "social_media": [],
        "git": "www.github.com",
        "bio": "I'm Shaz",
        "pic": "none",
        "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

    token = login("edd", "hunter2")["token"]
    set_profile(token, { 
        "display_name": "Edward Salkield",
        "email": "edward.salkield@gmail.com",
        "social_media": [],
        "git": "www.github.com",
        "bio": "I'm Edd",
        "pic": "none",
        "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})


    token = login("zuckerberg", "werunads")["token"]
    set_profile(token, { 
        "display_name": "Zucc",
        "email": "emperoroffacebook@gmail.com",
        "social_media": [],
        "git": "www.github.com",
        "bio": "I'm a robot",
        "pic": "none",
        "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

    create_hex("HackHarvard", "infinity", "test1", "modules/test.png", 10)
    create_hex("HackHarvard", "js", "test2", "modules/javascript.png", 10)
    create_hex("HackHarvard", "dino", "test3", "modules/offline-trex.png", 10)

    issue_tokens("shaz", "infinity", 10)
    issue_tokens("edd", "js", 10)
    issue_tokens("edd", "infinity", 10)
    issue_tokens("zuckerberg", "dino", 10)
    issue_tokens("zuckerberg", "infinity", 10)
    issue_tokens("zuckerberg", "js", 10)