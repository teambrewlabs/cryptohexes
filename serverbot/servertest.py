import requests, json
from modules.hexConv import *

IP = '10.0.0.4'
PORT = '8080'

def search(query_string, n_results):
    r = requests.post("http://" + IP + ':' + PORT + "/search", data = json.dumps({'query_string': query_string, 'n_results': n_results}))

    return(json.loads(r.content))
    


def register_user(username, password):
    r = requests.post("http://" + IP + ':' + PORT + "/register_user", data = json.dumps({
        'username': username,
        'password': password
    }))
    return(json.loads(r.content))


def register_organiser(username, password, token):
    r = requests.post("http://" + IP + ':' + PORT + "/register_organiser", data = json.dumps({
        'username': username,
        'password': password,
        'token': token
    }))
    return(json.loads(r.content))

    

def login(username, password):
    r = requests.post("http://" + IP + ':' + PORT + "/login", data = json.dumps({
        'username': username,
        'password': password
    }))
    return(json.loads(r.content))

def get_profile(username):
    r = requests.post("http://" + IP + ':' + PORT + "/get_profile", data = json.dumps({
        'username': username
    }))
    return(json.loads(r.content))
    

def set_profile(token, new_profile):
    r = requests.post("http://" + IP + ':' + PORT + "/set_profile", data = json.dumps({
        'token': token,
        'new_profile': new_profile
    }))
    return(json.loads(r.content))
    

def create_hack(token, new_hack):
    r = requests.post("http://" + IP + ':' + PORT + "/create_hack", data = json.dumps({
        'token': token,
        'new_hack': new_hack
    }))
    return(json.loads(r.content))
    
def get_hack(hack_name):
    r = requests.post("http://" + IP + ':' + PORT + "/get_hack", data = json.dumps({
        'hack_name': hack_name
    }))
    return(json.loads(r.content))


def get_hexes(username):
    r = requests.post("http://" + IP + ':' + PORT + "/get_hexes", data = json.dumps({
        'username': username
    }))
    return(json.loads(r.content))


def create_hex(organiser_name, hex_name, description, image_location, number):
    r = requests.post("http://" + IP + ':' + PORT + "/create_hex", data = json.dumps({
        'organiser_name': organiser_name,
        'hex_name': hex_name,
        'description': description,
        'image_location': image_location,
        'number': number
    }))
    return(json.loads(r.content))

def issue_tokens(username, asset, quantity):
    r = requests.post("http://" + IP + ':' + PORT + "/issue_tokens", data = json.dumps({
        'username': username,
        'asset': asset,
        'quantity': quantity
    }))
    return(json.loads(r.content))
