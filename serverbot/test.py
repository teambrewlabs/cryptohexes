from servertest import *


token = login("Edd", "hunter2")["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

token = login("Josh", "hunter2")["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})


token = login("George", "hunter2")["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

token = login("Shaz", "hunter2")["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})
