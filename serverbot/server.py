TOKEN_LENGTH = 20   # Token length in bytes
MAX_PROFILE_WRITES = 10 # The number of profile writes before non-overwritten information is ignored
CHAIN_NAME = 'cryptohex'
ONE_TIME_LOC = "./modules/otk.csv"
DEBUG = True


from pathlib import Path
import cherrypy
import Savoir, json
import modules.readRPC
import modules.searchchain
import modules.hexConv
import modules.sessions as sess
import modules.imageencode
import os, time, csv

# Crypto modules
import secrets
from Crypto.Cipher import AES
import hashlib

from Savoir import Savoir

(rpcuser, rpcpasswd) = modules.readRPC.read(str(Path.home()) + "/.multichain/" + CHAIN_NAME + "/multichain.conf")
rpchost = 'localhost'
rpcport = '2839'
sessions = sess.Sessions(10*60*60)

api = Savoir(rpcuser, rpcpasswd, rpchost, rpcport, CHAIN_NAME)
api.getinfo()


def new_encrypt(data, password):
    m = hashlib.md5()
    m.update(str.encode(data))
    key = m.digest()[:16]
    e_suite = AES.new(key, AES.MODE_CBC, 'IVIVIVIVIVIVIVIV')

    return e_suite.encrypt(data)

def new_decrypt(data, password):
    m = hashlib.md5()
    m.update(str.encode(data))
    key = m.digest()[:16]
    d_suite = AES.new(key, AES.MODE_CBC, 'IVIVIVIVIVIVIVIV')
    return d_suite.decrypt(data)

def encrypt(data, password):
    return data

def decrypt(data, password):
    return data


def errormsg(error_code, error_msg):
	return(json.dumps({'error_code': error_code , 'error_msg': error_msg}))

def success():
	return(json.dumps({'error_code': 0}))


def get_key_stream_structure(api, username):
    user_encrypted_key_structure = api.liststreamkeyitems("key", username)

    if len(user_encrypted_key_structure) == 0:
        return 1

    try:
        structure = modules.hexConv.json_to_dict(user_encrypted_key_structure[0]["data"])
    except Exception:
        return 2

    return structure

def get_hack_structure(api, hack_name):
    user_encrypted_key_structure = api.liststreamkeyitems("hack", hack_name)

    if len(user_encrypted_key_structure) == 0:
        return 1

    try:
        structure = modules.hexConv.json_to_dict(user_encrypted_key_structure[0]["data"])
    except Exception:
        return 2

    return structure


def get_hex_structure(api, hack_name):
    user_encrypted_key_structure = api.liststreamkeyitems("hex", hack_name)

    if len(user_encrypted_key_structure) == 0:
        return 1

    try:
        structure = modules.hexConv.json_to_dict(user_encrypted_key_structure[0]["data"])
    except Exception:
        return 2

    return structure


class RootServer:
    @cherrypy.expose
    def ping(self):
        return(success())

    @cherrypy.expose
    def search(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            query_string = str(body["query_string"])
            n_results = int(body["n_results"])
        except Exception:
            return errormsg(2, "Expected body element not found")
		
        # Perform the search
        print("Query: " + query_string)
        results = modules.searchchain.search(api, query_string, n_results)

        return(json.dumps({'error_code': 0, 'results': results}))


    @cherrypy.expose
    def register_user(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            username = str(body["username"])
            password = str(body["password"])
        except Exception:
            return errormsg(2, "Expected body element not found")

        # Test for user name uniqueness - remember to return user name, given
        if len(api.liststreamkeyitems('key', username)) != 0:
            return errormsg(5, "Username taken")

        # Generate a new address, giving it permission to connect and receive token
        new_addr = api.getnewaddress()
        api.grant(new_addr, "connect")
        api.grant(new_addr, "receive")
        api.grant(new_addr, "profile.write")
        api.grant(new_addr, "hack.write")
        api.grant(new_addr, "send")
        print("Created new user with address ", new_addr)

        # Generate key stream fields
        priv_key = api.dumpprivkey(new_addr)
        encrypted_key = encrypt(priv_key, password)
        encrypted_name = encrypt(username, password)
        print("Type of encrypted key:")
        print(type(encrypted_key))
        
        print("Type of encrypted name:")
        print(type(encrypted_name))

        print("Type of wallet_addr:")
        print(type(new_addr))

        # Create new entry in the key stream
        encoded = modules.hexConv.dict_to_json({
            'encrypted_key': encrypted_key,
            'encrypted_name': encrypted_name,
            'wallet_addr': new_addr
            })

        api.publish('key', username, encoded)

        print("Success!")
        return(success())


    @cherrypy.expose
    def register_organiser(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            username = str(body["username"])
            password = str(body["password"])
            otk = str(body["token"])
        except Exception:
            return errormsg(2, "Expected body element not found")
        

        # Test for user name uniqueness
        if len(api.liststreamkeyitems('key', username)) != 0:
            return errormsg(5, "Username taken")


        otks = [] # List to store otks and expiry dates
        found = False
        if (os.path.isfile(ONE_TIME_LOC)):
            with open(ONE_TIME_LOC) as f:
                for line in f:
                    otks.append(line.split(",")) # Read csv into otks
                    
            for o in otks:
                if otk in o[0] and time.time()-float(o[1])<0: 
                    # If we find the otk and it hasn't expired
                    found = True
                    print("Found the key!")
                    # Creat the new organiser
                    new_addr = api.getnewaddress() 
                    api.grant(new_addr, "connect") 
                    api.grant(new_addr, "receive") 
                    api.grant(new_addr, "send") 
                    print("Created new organiser with address ", new_addr)

                    # Rewrite the CSV without this otk
                    otks.remove(o)
            with open(ONE_TIME_LOC, 'w') as f:
                writer = csv.writer(f)
                for o in otks:
                    writer.writerow([s.rstrip() for s in o])
            if (not found):
                # Notify if did not find valid key
                return(errormsg(6, "Unable to find the one-time key " + otk))
        else:
            return(errormsg(6, "Unable to find the one-time key " + otk))




        # Generate key stream fields
        priv_key = api.dumpprivkey(new_addr)
        encrypted_key = encrypt(priv_key, password)
        encrypted_name = encrypt(username, password)

        # Create new entry in the key stream
        api.publish('key', username, modules.hexConv.dict_to_json({
            'encrypted_key': encrypted_key,
            'encrypted_name': encrypted_name,
            'wallet_addr': new_addr,
            'expiry': time.time() + 365*24*60*60
            }))

        return(success())


    @cherrypy.expose
    def login(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            username = str(body["username"])
            password = str(body["password"])
        except Exception:
            return errormsg(2, "Expected body element not found")

	# Get private key from username on the blockchain
        # TODO: make this actually encrypted

        structure = get_key_stream_structure(api, username)
        if type(structure) == int:
            return errormsg(8, "Unrecognised user, error code: " + str(structure))

        encrypted_key = structure['encrypted_key']
        encrypted_name = structure['encrypted_name']
        wallet_addr = structure['wallet_addr']

        # Decrypt the key and the username to confirm the user's password was correct1
        if not decrypt(encrypted_name, password) == username:
            return errormsg(7, "Incorrect password")

        priv_key = decrypt(encrypted_key, password)

        # Create new token, register it against the user
        token = secrets.token_hex(TOKEN_LENGTH)

        sessions.newsession(token)
        sessions.set(token, 'key', priv_key)
        sessions.set(token, 'wallet_addr', wallet_addr)
        sessions.set(token, 'username', username)

        print("New user with token:" + token)
        return(json.dumps({'error_code': 0, 'token': token}))




    @cherrypy.expose
    def get_profile(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            username = str(body["username"])
        except Exception:
            return errormsg(2, "Expected body element not found")

        # Look up key on the ledger
        structure = get_key_stream_structure(api, username)
        if type(structure) == int:
            return errormsg(8, "Unrecognised user")

        try:
            addr = structure['wallet_addr']
        except Exception:
            return errormsg(4, "Server data malformatted")

        # Find the profile associated with this key
        profile_list = api.liststreampublisheritems('profile', addr, False, MAX_PROFILE_WRITES)

        # Parse the profile list to find the most up-to-date profile

        profile = {}

        for p_entry in profile_list:
            try:
                body = json.loads(bytes.fromhex(p_entry['data']))
            except Exception:
                return errormsg("Server data malformatted")

            for k, v in body.items():
                profile[k] = v

        profile['error_code'] = 0
        return json.dumps(profile)


    # TODO: check integrity of uploaded profiles
    @cherrypy.expose
    def set_profile(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            token = str(body["token"])
            new_profile = body["new_profile"]
        except Exception:
            return errormsg(2, "Expected body element not found")

        # Verify the user is who they say they are 
        if not sessions.exists(token):
            return errormsg(9, "User not logged in")
        
        # Create new entry in the hack stream
        api.importprivkey(sessions.get(token, 'key'))
        api.publishfrom(sessions.get(token, 'wallet_addr'), 'profile', 
                        sessions.get(token, "username"), 
                        modules.hexConv.dict_to_json(new_profile))

        return(success())

        
    @cherrypy.expose
    def get_hexes(self):
        # Return a list of hex_names this user has
        # interacted with since the beginning of time
        hexes = []
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")
        try:
            username = str(body["username"]) # Load username to search for
        except Exception:
            return errormsg(2, "Expected body element not found")
        structure = get_key_stream_structure(api, username)
        if type(structure) == int:
            return errormsg(8, "Unrecognised user, error code: " + str(structure))
        wallet_addr = structure['wallet_addr'] # Get address of username
        if wallet_addr not in api.getaddresses():
            api.importaddress(wallet_addr) # Add the address if necesarry
        balances = api.getaddressbalances(wallet_addr)
        for balance in balances:
            hexes.append(balance["name"]) # Add the name of each hex user owns
        return(json.dumps({'error_code': 0, 'hexes': hexes}))
        

    @cherrypy.expose
    def create_hack(self):
        # User requests to create a hack
        # User specifies name of hack, collaborators,
        # organiser, repo and description
        # Verify the user through their password and add to the hack stream
        
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")
        try:
            token = str(body["token"])
            new_hack = body["new_hack"]

        except Exception:
            return errormsg(2, "Expected body element not found")

        if not DEBUG:
            if not sessions.exists(token):
                return errormsg(8, "Unrecognised user")

        # Check if collaborators and organiser are real
        # TODO: try block here in case malformatted hack
        if len(api.liststreamkeyitems('key', new_hack["organiser"], False, 1)) == 0:
            return errormsg(10, "Organiser not registered.")
        for collaborator in new_hack["collaborators"]:
            if len(api.liststreamkeyitems('key', collaborator, False, 1)) == 0:
                return errormsg(3, "User not registered.")

        # Create new entry in the hack stream
        if not DEBUG:
            api.importprivkey(sessions.get(token, 'key'))
            api.publishfrom(sessions.get(token, 'wallet_addr'), 'hack', new_hack["display_name"], 
                modules.hexConv.dict_to_json(new_hack))
        else:
            api.publish('hack', new_hack["display_name"], 
                modules.hexConv.dict_to_json(new_hack))

        return(success())



    @cherrypy.expose
    def issue_tokens(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            #token = str(body["token"])
            u_name = str(body["username"])
            ass = str(body["asset"])
            qty = int(body["quantity"])
        except Exception:
            return errormsg(2, "Expected body element not found")

        
        # if not sessions.exists(token):
        #     return errormsg(9, "User not logged in")

        # org_name = sessions.get(token, "username")

        # enc_key = sessions.get(token, "encrypted_key")
        # enc_name = sessions.get(token, "encrypted_name")
        # wall_add = sessions.get(token, "wallet_addr")

        # if not decrypt(enc_name, org_psswd) == org_name:
        #     return errormsg(7, "Incorrect password")

        # api.importaddress(decrypt(enc_key, org_psswd))
        try:
            user = api.liststreamkeyitems('key', u_name)[0]
        except Exception:
            return errormsg(3, "User not registered.")

        u_data = modules.hexConv.json_to_dict(user["data"])
        u_add = u_data["wallet_addr"]
        #api.sendassetfrom(wall_add, u_add, ass, qty)
        #api.issue()
        #api.issue(wallet_addr, hex_name, int(number))

        # issue qty of ass to my address TODO
        issuers = api.listpermissions('issue')
        this_issuer = issuers[0]["address"]
        api.issue(this_issuer, ass, qty)
        api.sendasset(u_add, ass, qty)

        return(success())


    @cherrypy.expose
    def create_hex(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            organiser_name = str(body["organiser_name"])
            hex_name = str(body["hex_name"])
            description = str(body["description"])
            image_location = str(body["image_location"])
            number = str(body["number"])
        
        except Exception:
            return errormsg(2, "Expected body element not found")

    # Create a new currency and issue to the organiser controlling it
    # This is called by the admin manually, so no need to verify the user name of the organiser
        print("1")

        with open(image_location, 'rb') as f:
            contents = f.read()
        print("2")
        
        image = modules.imageencode.image_to_hex(contents)

        print("Pre-publish")
        # Create new entry in the hex stream
        api.publish('hex', hex_name, modules.hexConv.dict_to_json({
            'description': description,
            'image': image,
            'number': number
            }))
        print("Post-publish")

        # Create the currency and issue to the organiser
        structure = get_key_stream_structure(api, organiser_name)
        if type(structure) == int:
            return errormsg(8, "Unrecognised user, error code: " + str(structure))
        wallet_addr = structure['wallet_addr']
        api.issue(wallet_addr, hex_name, int(number))
        return(success())
    

    @cherrypy.expose
    def get_hack(self):
        try:
            body = json.loads(cherrypy.request.body.read())
        except Exception:
            return errormsg(1, "Incorrectly encoded body")

        try:
            hack_name = str(body["hack_name"])
        except Exception:
            return errormsg(2, "Expected body element not found")

        
        structure = get_hack_structure(api, hack_name)
        if type(structure) == int:
            return errormsg(8, "Unrecognised user")


        structure['error_code'] = 0
        return json.dumps(structure)



    @cherrypy.expose
    def get_hex_image(self, hex_name):
        try:
            hex_name = str(hex_name)
        except Exception:
            return errormsg(2, "Expected body element not found")

        # Look up hex image on the hex ledger
        structure = get_hex_structure(api, hex_name)
        return modules.imageencode.hex_to_image(structure['image'])



if __name__ == '__main__':
    root=RootServer()
    config = {
            'global': {
                'server.socket_port': 8080,
                'server.socket_host': '10.0.0.4',
            },
            '/': {
                'tools.sessions.on': True,
                'tools.sessions.timeout': 10,
                }}
    cherrypy.quickstart(root, '/', config=config)
