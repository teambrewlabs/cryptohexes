# Functions to encode images into base 64 strings
import binascii

def image_to_hex(im):
    return binascii.hexlify(im).decode('utf-8')


def hex_to_image(hx):
    return binascii.unhexlify(hx.encode('utf-8'))

