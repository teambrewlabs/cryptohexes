#!/usr/bin/python3

import os, sys
import secrets
import time
import csv
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")
from modules import hexConv
from modules import imageencode

# Creates a new temporary token for Organiser registration
def newKeyOrg():
    # Generate one time key
    # Store it in the text file
    # Print it to the console

    otk = str(secrets.token_urlsafe(20))
    tstamp = time.time()

    # Adjust constant depending on how long otk should be valid for
    if (os.path.isfile('otk.csv')):
        with open('otk.csv', 'a') as f:
            f.write('\n' + otk + "," + str(tstamp + 60)) 
    else:
        with open('otk.csv', 'w') as f:
            f.write(otk + "," + str(tstamp + 60))
    print("Generated one-time-key ", otk)



# Prunes expired registration tokens from the database
def updateOtks():
    otks = []
    if (os.path.isfile('otk.csv')):
        with open('otk.csv') as f:
            for line in f:
                otks.append(line.split(",")) # Read csv into otks
                
        for o in otks:
            if time.time()-float(o[1])>0: 
                # The otk has expired so remove it
                otks.remove(o)
        with open('otk.csv', 'w') as f:
            writer = csv.writer(f)
            for o in otks:
                writer.writerow([s.rstrip() for s in o])
    else:
        print("No otks pending")


# Revokes permissions of Organisers with expired times
def updateOrgs():
    # Look through the list of users and revoke the permissions
    # of any organisers who no longer have a valid pass

    for transaction in api.liststreamitems(CryptoHex, count=99999):
        user_data = CBOR_to_dict(transaction["data"])
        if time.time() - user_data["expiry"] < 0:
            api.revoke(user_data["wallet_addr"], "send, receive")

def createHex(organiser_name, hex_name, description, image_location, number):
    # Create a new currency and issue to the organiser controlling it
    # This is called by the admin manually, so no need to verify the user name of the organiser

    with open(image_location, 'rb') as f:
        contents = f.read()
    
    image = modules.imageencode.image_to_hex(contents)

    # Create new entry in the hex stream
    api.publish('hex', hex_name, modules.hexConv.dict_to_json({
        'description': description,
        'image': image,
        'number': number
        }))

    # Create the currency and issue to the organiser
    structure = get_key_stream_structure(api, username)
    if type(structure) == int:
        return errormsg(8, "Unrecognised user, error code: " + str(structure))
    wallet_addr = structure['wallet_addr']
    api.issue(wallet_addr, hex_name, number)
