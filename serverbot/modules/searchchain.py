from fuzzywuzzy import fuzz
from operator import itemgetter
from modules import hexConv
import json

# Given a query and list of choices in the form [()
def top_results(query, choices, n_results):
    results = list(map(lambda c: (c[0], c[1], c[2], fuzz.ratio(query, c[1])), choices))
    results = sorted(results, key=itemgetter(3), reverse=True)
    results = list(map(lambda r: r[:3], results))
    return results[:n_results]

def get_data(api, stream_name):
    keys = list(map(lambda item: item['key'], api.liststreamkeys(stream_name)))
    try:
        data = list(map(lambda key: (key, hexConv.json_to_dict(api.liststreamkeyitems(stream_name, key, False, 1)[0]['data'])['display_name'], stream_name), keys))
    except Exception as e:
        print(e) # TODO: error handling
    else:
        return data

def search(api, query, n_results):
    hacks = get_data(api, 'hack')
    profiles = get_data(api, 'profile')
    organisers = get_data(api, 'organiser')

    return top_results(query, hacks + profiles + organisers, n_results)
