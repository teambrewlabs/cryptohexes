def read(loc):
    lines = open(loc).readlines()
    for line in lines:
        if line.startswith("rpcuser="):
            rpcuser = line[len("rpcuser="):-1]
        if line.startswith("rpcpassword="):
            rpcpasswd = line[len("rpcpassword="):-1]
    return((rpcuser,rpcpasswd))

