import json, binascii

# Convert python structure to hex and back
def json_to_dict(cb):
    return json.loads((binascii.unhexlify(cb.encode('utf-8'))).decode('utf-8'))

def dict_to_json(d):
    return binascii.hexlify(json.dumps(d).encode('utf-8')).decode('utf-8')
