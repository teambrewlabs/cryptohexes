# Sessions class that handles persistent memory-based sessions without requiring browser cookies

import time

class Sessions:
    sessions = {}   # Maps token to the "session" dict, and a timestamp
    timeout = 60*10

    def __init__(self, timeout=60*10):
        self.timeout = timeout  #Number of seconds until the token expires

    def newsession(self, token):
        self.sessions[token] = {
            "session": {},
            "timeout": time.time() + self.timeout
        }

        return self.sessions[token]

    def exists(self, token):
        try:
            self.sessions[token]
            return True
        except Exception:
            return False
    
    def get(self, token, key):
        try:
            sesh = self.sessions[token]
        except Exception:
            return None

        try:
            if sesh["timeout"] < time.time():
                return None
        except Exception:
            return None

        try:
            return sesh["session"][key]
        except Exception:
            return None

    def set(self, token, key, value):
        try:
            sesh = self.sessions[token]
        except Exception:
            return None

        try:
            if sesh["timeout"] < time.time():
                return None
        except Exception:
            return None

        try:
            sesh["session"][key] = value
            return 0
        except Exception:
            return None
    


