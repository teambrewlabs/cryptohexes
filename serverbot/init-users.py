from servertest import *

register_user("Edd", "hunter2")
register_user("Josh", "hunter2")
register_user("Shaz", "hunter2")
register_user("George", "hunter2")

token = input("Give me a valid one-time token: ")
register_organiser("HackHarvard", "password", token)

r = login("Edd", "hunter2")
token = r["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

r = login("Josh", "hunter2")
token = r["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

r = login("Shaz", "hunter2")
token = r["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

r = login("George", "hunter2")
token = r["token"]
set_profile(token, { 
    "display_name": "Edward Salkield",
    "email": "edward.salkield@gmail.com",
    "social_media": [],
    "git": "www.github.com/EdwardSalkield",
    "bio": "I'm Edd",
    "pic": "none",
    "hacks": [("CryptoHex", "CryptoHex", "HackHarvard")]})

create_hack(token, {
        "display_name": "CryptoHex",
        "collaborators": ["Josh", "Shaz", "George"],
        "organiser": "HackHarvard",
        "repo": "https://gitlab.com/teambrewlabs/cryptohexes",
        "description": "Dummy project"
    })


# Create user profiles


