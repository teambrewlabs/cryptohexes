
search
register_user
register_organiser
login
get_profile
set_profile
create_hack






######### API Function Calls #########

ERROR REPORTING
Every response contains an error code, and optionally an error message:
{
    error_code: Integer,
    error_msg: String
}

Error codes
0 - success
1 - incorrectly encoded body
2 - expected body element not found
3 - user not registered
4 - server data malformatted
5 - username taken
6 - unable to find the one-time key
7 - incorrect password
8 - unrecognised user


    error_msg: String
    error_code: Integer



/api/search
Returns the best nresults matches over the profile stream, with key queryString

->
{
    query_string: String,
    n_results: Integer
}

<-
{
    results: [(String: name, String: display_name, String: type)], // Type is "hack", "profile" or "organiser"
}




/api/register_user

->
{
    username: String,
    password: String
}

<-
Success (error code 0)


/api/login
->
{
    username: String,
    password: String
}

<-
{
    token: String
}


/api/get_profile
->
{
    username: String
}

<-
{
    display_name: String,
    email: String,
    social_media: [String],
    git: [String],
    bio: String,
    pic: Image,
    hackathons: [(String display_name, String user_name)]],
    hacks: [(String display_name, String hack_name, String organiser_name, String organiser_display_name, Boolean verified)]
}


/api/set_profile
->
{
    token: String,  // Confirm the user is logged in
    new_profile: {
        display_name: String,
        email: String,
        social_media: [String],
        git: [String],
        bio: String,
        pic: Image,
        hacks: [(String display_name, String hack_name, String organiser_name)]
    }    
}

/api/get_hexes
->
{
    username: String
}

<-
{
    hexes: [String]
}



/api/create_hack
->
{
    token: String,
    new_hack: {
        display_name: String,
        collaborators: [String], // List of user_name (NOT display_name)
        organiser: String,
        repo: String,
        description: String
    }
}


<-
{

/api/get_hex_image
->
{
	hex_name: String
}

<-
hex.png


/api/get_hack
->
{
    hack_name: String
}

<-
{
    display_name: String,
    collaborators: [String], // List of user_name (NOT display_name)
    organiser: String,
    repo: String,
    description: String
}

/api/issue_tokens
->
{
    username : String
    asset : String
    quantity: Int
}
<-
{
    Success (error code 0)
}
    

################ Streams ###############

* key
 - Purpose: Maps a user_name or organiser_name to immutable properties about that user/organiser.
 - Permissions: Editable by admin (bot) only
 - Key: String user_name/organiser_name
 - Body:
    {
        encrypted_key: String,
        encrypted_name: String, // user_name, not display_name
        wallet_addr: Bytes,
        expiry: Integer         // For organisers only, UNIX time beyond which cannot transfer assets
    }


* profile
 - Purpose: Maps user_name to its profile information
 - Permissions: Any users can write
 - Key: String user_name
 - Body (JSON encoded): 
    {
        display_name: String,
        email: String,
        social_media: [String],
        git: [String],
        bio: String,
        pic: Image,
        hacks: [(String display_name, String hack_name, String organiser_name)]
    }    


* hack
 - Purpose: Maps hack_name to the hack information
 - Permissions: Editable by organisers and users
 - Key: hack_name
 - Body (JSON encoded):
    {
        display_name: String,
        collaborators: [String], // List of user_name (NOT display_name)
        organiser: String,
        repo: String,
        description: String
    }

* organiser
 - Purpose: Maps organiser_name to the organiser profile information
 - Permissions: Editable by organisers
 - Key: organiser_name
 - Body: (JSON encoded):
    {
        display_name: String,
        email: String,
        social_media: [String],
        git: [String],
        bio: String,
        pic: Image,
        hacks: [(String display_name, String hack_name, String organiser_name)]
    }    

* hex
 - Purpose: Maps names of different currencies to their information
 - Permissions: Editable only by admin
 - Key: hex_name
 - Body: (JSON encoded):
     {
         num_issued: String,
         description: String,
         image: String
     }
